#!/usr/bin/env R
##
## This file is part of chainy.
##
## chainy is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## chainy is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with chainy.  If not, see <http://www.gnu.org/licenses/>.

## equations start

output$equation1 <- reactive({
    code <- '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mover accent="true"><mrow><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover><mo>=</mo><mfrac linethickness="1"><mrow><mstyle displaystyle="true"><munderover><mo>&sum;</mo><mrow><mi>i</mi><mo>=</mo><mn>1</mn></mrow><mi>n</mi></munderover></mstyle><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi><mi>i</mi></mrow></msub></mrow><mi>n</mi></mfrac></mrow></math>'
    
    return(code)
})

output$equation2 <- reactive({
    code <- '
<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mover accent="true"><mrow><msub><mi>E</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover><mo>=</mo><mfrac linethickness="1"><mrow><mstyle displaystyle="true"><munderover><mo>&sum;</mo><mrow><mi>i</mi><mo>=</mo><mn>1</mn></mrow><mi>n</mi></munderover></mstyle><msub><mi>E</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi><mi>i</mi></mrow></msub></mrow><mi>n</mi></mfrac></mrow></math>
'
    return(code)
})

output$equation3 <- reactive({
    code <- '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mover accent="true"><mrow><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover><mo>=</mo><mfrac linethickness="1"><mrow><mstyle displaystyle="true"><munderover><mo>&sum;</mo><mrow><mi>k</mi><mo>=</mo><mn>1</mn></mrow><mi>s</mi></munderover></mstyle><mover accent="true"><mrow><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover></mrow><mi>s</mi></mfrac></mrow></math>'

    return(code)
})


output$equation4 <- reactive({
    code <- '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mover accent="true"><mrow><msub><mi>E</mi><mrow><mi>j</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover><mo>=</mo><mfrac linethickness="1"><mrow><mstyle displaystyle="true"><munderover><mo>&sum;</mo><mrow><mi>k</mi><mo>=</mo><mn>1</mn></mrow><mi>s</mi></munderover></mstyle><mover accent="true"><mrow><msub><mi>E</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover></mrow><mi>s</mi></mfrac></mrow></math>'

    return(code)
})

output$equation5 <- reactive({
    code <- '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mi>&Delta;</mi><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub><mo>=</mo><mover accent="true"><mrow><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover><mo>-</mo><mover accent="true"><mrow><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover></mrow></math>'
    return(code)
})

output$equation6 <- reactive({
    code <-  '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mi>R</mi><msub><mi>Q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub><mo>=</mo><msup><mrow><mover accent="true"><mrow><msub><mi>E</mi><mrow><mi>j</mi><mi>l</mi></mrow></msub></mrow><mo stretchy="true">&OverBar;</mo></mover></mrow><mrow><mi>&Delta;</mi><mi>C</mi><msub><mi>q</mi><mrow><mi>j</mi><mi>k</mi><mi>l</mi></mrow></msub></mrow></msup></mrow></math>'
    return(code)
})

## equations end
