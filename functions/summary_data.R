#!/usr/bin/env R
##
## This file is part of chainy.
##
## chainy is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## chainy is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with chainy.  If not, see <http://www.gnu.org/licenses/>.


## dd <- read.table("/imppc/labs/maplab/share/chainy/relative_quant/testQAlu.csv", sep = "\t", stringsAsFactors = FALSE, header = TRUE)

##' <description>
##'
##' <details>
##' @title 
##' @param dd 
##' @param results 
##' @return 
##' @author Anna Díez
summary_data <- function(dd, results){
  dd2 <- dd[dd$included,]
  removed <- dim(dd)[1] - dim(dd2)[1]
  
  refer <- dd2[grep("r", dd2$group, perl = TRUE),]
  refer <- unique(refer$primers)
  
  genes <- dd2[grep("g", dd2$group, perl = TRUE),]
  genes <- unique(genes$primers)
  
  treat <- dd2[grep("s", dd2$group, perl = TRUE),]
  treat <- unique(treat$treatment)
  
  controls <- dd2[grep("c", dd2$group, perl = TRUE),]
  controls <- unique(controls$treatment)
  
  cat("number of wells removed from the analysis: \n", paste0(capture.output(removed), "\n"),
      "\nnumber of wells included in the analysis: \n", paste0(capture.output(dim(dd)[1] - removed), "\n"),
      "\ngenes of interest: \n", paste0(capture.output(genes), "\n"),
      "\nreferences: \n", paste0(capture.output(refer), "\n"),
      "\ntreatments: \n", paste0(capture.output(treat), "\n"),
      "\ncontrols: \n", paste0(capture.output(controls), "\n"),
      "\ncomparisons: \n", paste0(capture.output(colnames(results)), "\n"))    
}
