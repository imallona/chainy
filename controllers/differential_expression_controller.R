#!/usr/bin/env R
##
## This file is part of chainy.
##
## chainy is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## chainy is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with chainy.  If not, see <http://www.gnu.org/licenses/>.

source(file.path('functions', 'relative_quantification.R'))
source(file.path('functions', 'stability.R'))

METHOD <- 'Perm'

`%then%` <- shiny:::`%OR%`

output$de_panel_browsing_buttons <- renderUI({
    if ((is.null(user_design()) | is.null(cq_eff()) |
             is.null(input$kinetic_method) | is.null(input$ref_mean) | is.null(input$type_eff))
        & is.null(user_design_use_case_cq_eff())) {

        return(NULL)
    }
    
    tagList(actionButton("de_tabpanel_prev", label = "Previous step",
                         additionalClass = "btn-primary"),
            downloadButton('zip_report', label = 'Download report'))
    
})

observe({
    if (is.null(input$de_tabpanel_prev))
        return(NULL)
    if (input$de_tabpanel_prev == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'check_tabpanel')
})

estimate_time_to_complete <- function(group) {
    ## seconds to compute a single comparison
    unit <- 0.35

    RCs <- sort(unique(grep("r\\d*c\\d*", group, perl = TRUE, 
                            value = TRUE)))
    GCs <- sort(unique(grep("g\\d*c\\d*", group, perl = TRUE, 
                            value = TRUE)))
    RSs <- sort(unique(grep("r\\d*s\\d*", group, perl = TRUE, 
                            value = TRUE)))
    GSs <- sort(unique(grep("g\\d*s\\d*", group, perl = TRUE, 
                            value = TRUE)))
    if (length(RCs) > 0 && length(RSs) > 0) 
        hasRef <- TRUE
    else hasRef <- FALSE
    
    if (hasRef) 
        COMBS <- expand.grid(GCs, GSs, RCs, RSs, stringsAsFactors = FALSE)
    else
        COMBS <- expand.grid(GCs, GSs, stringsAsFactors = FALSE)

    Cnum <- t(apply(COMBS, 1, function(x) gsub("[rgs]\\d*", "",  x, perl = TRUE)))
    Snum <- t(apply(COMBS, 1, function(x) gsub("[rgc]\\d*", "",  x, perl = TRUE)))
    Gnum <- t(apply(COMBS, 1, function(x) gsub("[rsc]\\d*", "",  x, perl = TRUE)))
    Rnum <- t(apply(COMBS, 1, function(x) gsub("[gsc]\\d*", "",  x, perl = TRUE)))
    Cnum <- t(apply(Cnum, 1, function(x) x[x != ""]))
    Snum <- t(apply(Snum, 1, function(x) x[x != ""]))
    Gnum <- t(apply(Gnum, 1, function(x) x[x != ""]))
    Rnum <- t(apply(Rnum, 1, function(x) x[x != ""]))
    
    combs <- 'same'
    
    if (hasRef) {      
        SELECT <- which(Cnum[, 1] == Cnum[, 2] & Snum[, 1] == 
                            Snum[, 2] & Gnum[, 1] == Gnum[, 2] & Rnum[, 1] == 
                                Rnum[, 2])
        COMBS <- COMBS[SELECT, ]
    }
    else {
        SELECT <- which(Gnum[, 1] == Gnum[, 2])
        COMBS <- COMBS[SELECT, ]        
    }
    
    return(sprintf('Processing relative expression....\nExpected time to compute %s comparisons (and the associated 100 permutations for each one): %s s.\nPlease do not reload nor go back until finished.',
                   nrow(COMBS),
                   nrow(COMBS) * unit))
}



calculate_relative_expression <- reactive({
    if (input$use_case_example > 0) {
        progress <- shiny::Progress$new()
        on.exit(progress$close())
        progress$set(message = "Processing relative expression...",
                     detail = 'This may take a while...', value = 0)
        
        dd <- merge_cqs_and_user_design()
        lde <- relative_quantification_parsed(dd = dd,
                                              refmean = input$ref_mean,
                                              method = METHOD,
                                              type_eff = input$type_eff)
    }
    else if (input$use_case_cq_eff == 0) {
        input$design_ii_tabpanel_next
        progress <- shiny::Progress$new()
        on.exit(progress$close())
        validation <- validate_user_design()
        
        validate_relative_de(validation)
        validate_design_completness(validation)
        
        dd <- validation$dat
        
        progress$set(message = estimate_time_to_complete(dd$group), value = 0)

        lde <- relative_quantification_parsed(dd = dd,
                                              refmean = input$ref_mean,
                                              method = METHOD,
                                              type_eff = input$type_eff)
    } else if (input$use_case_cq_eff > 0) {
        input$design_ii_tabpanel_use_case_cq_eff_next
        progress <- shiny::Progress$new()
        on.exit(progress$close())
        
        validation <- validate_user_design_use_case_cq_eff()
        
        validate_relative_de(validation)
        validate_design_completness(validation)
        dd <- validation$dat
        
        progress$set(message = estimate_time_to_complete(dd$group), value = 0) 
        
        lde <- relative_quantification_parsed(dd = dd,
                                              refmean = input$ref_mean,
                                              method = METHOD,
                                              type_eff = input$type_eff)
    }

    return(lde)      
})

refresh_stability_input <- reactive({

    input$design_ii_tabpanel_next
    input$design_ii_tabpanel_use_case_cq_eff_next
    
    if (input$use_case_cq_eff == 0) {
        validation <- validate_user_design()
        validate_reference_stability(validation)
        validate_design_completness(validation)

        dat <- validation$dat        
        dat <- dat[grep("^r", dat$group),]
        
        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                       cq = dat$cq,
                       efficiency = dat$efficiency,
                       included = dat$included,
                       group = dat$group,
                       primers = dat$primers,
                       treatment = dat$treatment,
                       row.names = NULL))

        return(stab_input)
    }
    
    else if (input$use_case_cq_eff > 0) {
        validation <- validate_user_design_use_case_cq_eff()
        validate_reference_stability(validation)
        validate_design_completness(validation)

        dat <- validation$dat        
        dat <- dat[grep("^r", dat$group),]

        save(validation, file = '/tmp/debug')
        
        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                       cq = dat$cq,
                       efficiency = dat$efficiency,
                       included = dat$included,
                       group = dat$group,
                       primers = dat$primers,
                       treatment = dat$treatment,
                       row.names = NULL))

        return(stab_input)
    }        
})


refresh_scaled_quantities_input <- reactive({
    if (input$use_case_cq_eff == 0 ) {
        ## dat <- merge_cqs_and_user_design()

        validation <- validate_user_design()
        
        validate_scaled_quantities(validation)
        
        dat <- validation$dat
        
        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                       cq = dat$cq,
                       efficiency = dat$efficiency,
                       included = dat$included,
                       group = dat$group,
                       primers = dat$primers,
                       treatment = dat$treatment,
                       row.names = NULL))
    } else if (input$use_case_cq_eff > 0 ) {
        ## dat <- validate_user_design_use_case_cq_eff()$dat

        validation <- validate_user_design_use_case_cq_eff()
        validate_scaled_quantities(validation)
        
        dat <- validation$dat
        
        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                       cq = dat$cq,
                       efficiency = dat$efficiency,
                       included = dat$included,
                       group = dat$group,
                       primers = dat$primers,
                       treatment = dat$treatment,
                       row.names = NULL))
    }

    return(stab_input)
})




refresh_cq_eff_plot_input <- reactive({
    if (input$use_case_cq_eff == 0 ) {
        ## dat <- merge_cqs_and_user_design()

        validation <- validate_user_design()
        validate_cq_eff_plot(validation)
        
        dat <- validation$dat
        
        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                       cq = dat$cq,
                       efficiency = dat$efficiency,
                       included = dat$included,
                       group = dat$group,
                       primers = dat$primers,
                       treatment = dat$treatment,
                       row.names = NULL))
    } else if (input$use_case_cq_eff > 0 ) {
        ## dat <- validate_user_design_use_case_cq_eff()$dat

        validation <- validate_user_design_use_case_cq_eff()
        validate_cq_eff_plot(validation)
        
        dat <- validation$dat
        
        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                       cq = dat$cq,
                       efficiency = dat$efficiency,
                       included = dat$included,
                       group = dat$group,
                       primers = dat$primers,
                       treatment = dat$treatment,
                       row.names = NULL))
    }

    return(stab_input)
})


calculate_stability <- reactive({

    input$design_ii_tabpanel_next
    input$design_ii_tabpanel_use_case_cq_eff_next

    ## validation <- validate_user_design()
    ## validate_reference_stability(validation)
    
    progress <- shiny::Progress$new()
    on.exit(progress$close())
    progress$set(message = "Processing stability...\nPlease do not reload nor go back until finished.", value = 0)
    
    
    stab_input <- refresh_stability_input()

    ## ddrq <- stability(na.omit(stab_input))
    ddrq <- get_expression_quantities(na.omit(stab_input))[['q']]
    hk <- select_references(ddrq)
    
    return(list(ddrq = ddrq, hk = hk))
    
})

output$differential_expression_plot <- renderPlot({
    plot_relative_quantification(calculate_relative_expression()[['parsed']])
})


output$differential_expression_renderedimage <- renderImage({
    lde <<- calculate_relative_expression()
    
    outfile <- tempfile(fileext='.png')

    ## Generate a png
    num_groups <- ncol(lde[['parsed']])
    adjusted_height <- 1000
    if (num_groups < 10) {
        adjusted_height <- 60* num_groups
    } else {
        adjusted_height <- 30 * num_groups
    }

    tryCatch({
        png(outfile, width = 1000,
            height = adjusted_height )
        plot_relative_quantification(lde[['parsed']])    
        dev.off()
        ## to catch margins to large cases (refmeaning small experiments)
    }, error = function(x) {
        png(outfile, width = 1000)
        plot_relative_quantification(lde[['parsed']])
        dev.off()
    })

    ## Return a list
    list(src = outfile,
         alt = "Differential_expression_output")
}, deleteFile = TRUE)


output$differential_expression_dt <- renderDataTable({
    lde <- calculate_relative_expression()

    ## moving variable names, rownames, to the first column
    cnames <- colnames(lde[['parsed']])
    rnames <- rownames(lde[['parsed']])
    
    rounded <- apply(lde[['parsed']], 2, function(x) round(x,2))
    rounded <- rbind(rounded, rep(NA, ncol(rounded)))
    rownames(rounded)[nrow(rounded)]<- 'status'
    
    for (i in 1:ncol(rounded)) {
        if ((as.numeric(rounded["perm < init", i])) < 0.05 & as.numeric(rounded["perm == init", i]) != 1)
            rounded['status', i] <- "down"
        else if ((as.numeric(rounded["perm > init", i])) < 0.05 & as.numeric(rounded["perm == init", i]) != 1)
            rounded['status', i] <- "up"
        else
            rounded['status', i] <- "ns"
    }
    
    ## removing the perm == init element, as it might be counterintuitive
    out <- data.frame(cbind(contrast = cnames, t(rounded)))
    out <- out[,-9]
    
    colnames(out) <- c('contrast', 'Mean', 'Std.dev', 'Median', 'MAD', 'Conf.lower',
                       'Conf.upper', 'perm > init', 'perm < init', 'status')

    return(out)
})

output$stability_m <- renderPlot({
    refresh_stability_input()

    stabilityM(hk = calculate_stability()$hk, ddrq = calculate_stability()$ddrq)
})

output$stability_v <- renderPlot({
    refresh_stability_input()
    
    stabilityV(hk = calculate_stability()$hk)
})


output$stability_summary_text <- renderPrint({
    stability_summary(hk = calculate_stability()$hk)
})

output$de_csv <- downloadHandler(
    filename = function() {
        sprintf('differential_expression_%s.csv', get_timestamp())
    },
    content = function(file) {
        write.csv(x = data.frame(calculate_relative_expression()[['parsed']]), file = file, quote = FALSE)
    })

output$de_stability_assessment <- renderUI({
    if (input$use_case_cq_eff == 0) {        
        tagList(   
            uiOutput('stability_summary_html'),
            fluidRow(
                column(width = 6,
                       h4('M values'),
                       plotOutput('stability_m')#,
                       ),
                column(width = 6,
                       h4('PV values'),
                       plotOutput('stability_v')#,
                       ))) #,
    }
    else if (input$use_case_cq_eff > 0 |
             input$design_ii_tabpanel_use_case_cq_eff_next > 0) {
        
        tagList(
            fluidRow(
                column(width = 6,
                       h4('M values'),
                       plotOutput('stability_m')#,
                       ## downloadButton('stability_m_png', label = 'Download as PNG'),
                       ## downloadButton('stability_m_pdf', label = 'Download as PDF')
                       ),
                column(width = 6,
                       h4('PV values'),
                       plotOutput('stability_v')#,
                       ## downloadButton('stability_v_png', label = 'Download as PNG'),
                       ## downloadButton('stability_v_pdf', label = 'Download as PDF'
                       )),    
            ## verbatimTextOutput("stability_summary_text"),
            uiOutput('stability_summary_html'))#,
        ## actionButton('report', label = 'Download report',
        ##              additionalClass = 'btn-info'))
        ## dataTableOutput('simplified_stability_dt'))
        ## dataTableOutput('simplified_stability_dt'))
    }
})

                                        #@ deprecated
output$stability_summary_html <- renderUI({
    input$design_ii_tabpanel_next
    input$design_ii_tabpanel_use_case_cq_eff_next    
    tagList(
        br(),
        h4('Reference stability assessment'),
        ## out$stable,
        fluidRow(
            renderDataTable(stability_self_explaining_table(hk = calculate_stability()$hk))
        )
    )
})
