#!/usr/bin/env R
##
## This file is part of chainy.
##
## chainy is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## chainy is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with chainy.  If not, see <http://www.gnu.org/licenses/>.

source(file.path('functions', 'cq_eff_plot.R'))
source(file.path('functions', 'stability.R'))
source(file.path('functions', 'validation.R'))

output$visual_inspection <- renderUI({
    if (input$use_case_example > 0)
        return(
            tagList(
                h4('Cq vs efficiency check plot'),
                fluidRow(
                    column(width = 9,
                           plotOutput('pce_plot')
                           
                           ),
                    column(width = 3,
                           checkboxInput(inputId = "show_pce_labels",
                                         label = "Show names",
                                         value = FALSE)
                           )
                    ),
                h4('Scaled quantities'),
                plotOutput('scaled_quantities_plot'),
                dataTableOutput('scaled_quantities_dt'),

                actionButton("check_tabpanel_prev", label = "Previous step",
                             additionalClass = "btn-primary"),
                actionButton("check_tabpanel_next", label = "Next step",
                             additionalClass = "btn-primary") 
                )
    )

    if ((is.null(user_design()) | is.null(cq_eff()))
        &  is.null(user_design_use_case_cq_eff())) 

        return(NULL)
    
    else
        return(tagList(
            h4('Cq vs efficiency check plot'),
            fluidRow(
                column(width = 9,
                       plotOutput('pce_plot')
                       
                       ),
                column(width = 3,
                       checkboxInput(inputId = "show_pce_labels",
                                     label = "Show names",
                                     value = FALSE)
                       )
            ),
            h4('Scaled quantities'),
            ## plotOutput('scaled_quantities_plot'),
            dataTableOutput('scaled_quantities_dt'),

            actionButton("check_tabpanel_prev", label = "Previous step",
                         additionalClass = "btn-primary"),
            actionButton("check_tabpanel_next", label = "Next step",
                         additionalClass = "btn-primary")
            ))
})




observe({
    if (is.null(input$check_tabpanel_next))
        return(NULL)
    if (input$check_tabpanel_next == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'de_tabpanel')
})

observe({
    if (is.null(input$check_tabpanel_prev))
        return(NULL)
    if (input$check_tabpanel_prev == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'design_tabpanel')
})



output$pce_plot <- renderPlot({
    stab_input <- refresh_cq_eff_plot_input()
    pce_cq_eff_plot(stab_input = na.omit(stab_input),
                    points = !input$show_pce_labels)                
})



plot_relative_expression_quantities <- function(q) {

    dat <- list()
    dat[['nrqs']] <- t(q[['q']])
    dat[['nrqs.se']] <- t(q[['q.sd']])

    par(mai = par()$mai + c(0,0,0,1), lwd = 2)
    
    ## if no replicates, sd are 0 and the sd span should be corrected
    ## not to be infinite
    sd_span <- suppressWarnings(ifelse(is.finite(max(dat[['nrqs.se']], na.rm = TRUE)),
                      max(dat[['nrqs.se']], na.rm = TRUE),
                      0))
    ymax <- max(dat[['nrqs']], na.rm = TRUE) + sd_span + 0.2    
    ymin <- 0
    
    bp <- barplot(dat[['nrqs']], beside = TRUE, lwd = 2,
                  legend.text = TRUE,
                  args.legend = list( 
                      x = "topright",
                      ## x = "bottom",
                      ## inset = -0.2,
                      cex = .7),
                  ylab = 'scaled relative quantities (a.u.)',
                  ylim = c(ymin, ymax))    

    segments(bp, dat[['nrqs']] - dat[['nrqs.se']]/2 , bp,
             dat[['nrqs']] +  dat[['nrqs.se']]/2, lwd = 2, ylim = c(ymin, ymax))

    arrows(bp, dat[['nrqs']] - dat[['nrqs.se']]/2 , bp,
           dat[['nrqs']] +  dat[['nrqs.se']]/2, lwd = 2,
           angle = 90,
           code = 3, length = 0.05,
           ylim = c(ymin, ymax))
    
}

output$scaled_quantities_plot <- renderPlot({
    q <-  get_expression_quantities(refresh_scaled_quantities_input())
    plot_relative_expression_quantities(q)
})



output$scaled_quantities_dt <- renderDataTable({
    q <-  get_expression_quantities(refresh_scaled_quantities_input())
    
    ## renaming colnames from, for instance, sd.act to act.stdev
    colnames(q[['q.sd']]) <- paste0(substring(colnames(q[['q.sd']]), 4, nchar(colnames(q[['q.sd']]))), '.sd')
    colnames(q[['q']]) <- paste0(colnames(q[['q']]), '.mean')
    merged <- cbind(q[['q']], q[['q.sd']])    
    ## merged <- round(merged[,order(names(merged))], 2)
    merged <- signif(merged[,order(names(merged))], 2)

    ## preppending the rownames
    q_names <- colnames(merged)
    merged$treatment <- row.names(merged)    
    merged <- merged[ ,c('treatment', q_names)]
    
    return(merged)    
}, options = list(pageLength = 5))
