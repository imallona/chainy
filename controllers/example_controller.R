#!/usr/bin/env R
##
## This file is part of chainy.
##
## chainy is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## chainy is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with chainy.  If not, see <http://www.gnu.org/licenses/>.

source(file.path('functions', 'relative_quantification.R'))
source(file.path('functions', 'chooser.R'))
source(file.path('functions', 'raw_fluorescence_processing.R'))
source(file.path('functions', 'stability.R'))

METHOD <- "Perm"

data <- reactive({
    in_file <- file.path('data', 'vermeulen_raw_data.delim')

    parsed_dat <- parse_input_delim(in_file)

    return(parsed_dat)
})

observe({
    if (input$fluo_load_next == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'eff_cqs_tabpanel')
})

observe({
    if (input$use_case_example > 0 & input$eff_cqs_tabpanel_next > 0)
        updateTabsetPanel(session, 'main', selected = 'check_tabpanel')
})


output$file_uploaded <- reactive({
    return(!is.null(data()))
})

outputOptions(output, 'file_uploaded', suspendWhenHidden = SUSPENDWHENHIDDEN)

output$check_format <- renderText({
    tryCatch({
        if (length(data()) > 0)
            return('<span class="label label-success">Success</span> The fluorescence data have been parsed')
        else if (!is.null(input$fluo))
            return(sprintf('<span class="label label-danger">Error</span> The fluorescence data do not fit to the format you selected (%s)', input$thermocycler))
        else
            return('<span class="label label-info">Info</span> Please upload a fluorescence file')
    })#, error = function(e) NULL)
})

output$data_table_raw_input <- renderDataTable({
    dat <- data()
}, options = list(iDisplayLength = 10))


## cq and efficiency sum up table

cq_eff <- reactive({
    if(is.null(data()))
        return(NULL)      
    ## cq_eff <- batch_using_modlist(data(), fit_method())    
    ## cq_colname <- get_cq_column_name(fit_method())
    ## editable <- cbind(names = rownames(cq_eff), cq_eff[,c(cq_colname, 'sig.eff')])
    load(file.path('data', sprintf('example_editable_%s.RData', fit_method())))
    
    return(editable)    
})


output$batch_cq_eff_dt <- renderDataTable({
    if(is.null(data()))
        return(NULL)

    progress <- shiny::Progress$new()
    on.exit(progress$close())
    progress$set(message = "Processing...", value = 0)
        
    d <- cq_eff()
    d[is.na(d)] <- "NA"

    return(d)
    
}, options = list(iDisplayLength = 10))

output$curr_plot <- renderPrint({
    input$plot_next - input$plot_prev
})

output$curr_plot_name <- renderText({
    curr_plot <- input$plot_next - input$plot_prev

    if(curr_plot > -1)
        return(colnames(data())[curr_plot + 2])
    else
        return(colnames(data())[2])
})

output$kinetic_fit_plot <- renderPlot({    
    curr_plot  <- input$plot_next - input$plot_prev + 2
    if (curr_plot > 1)
        per_well_lightcycler(data(), curr_plot, fit_method())
    else
        per_well_lightcycler(data(), 2, fit_method())
})


output$download_batch_cq_eff <- downloadHandler(
    filename = function() {
        paste0('batch_cq_eff-', Sys.Date(), '.csv')
    },
    content = function(file) {
        write.csv(x = data.frame(cq_eff()), file = file, quote = FALSE)
    })

output$thermocycler <- renderPrint(input$thermocycler)

## kinetic plotting of effs and cqs start

output$curr_plot <- renderPrint({
    input$plot_next - input$plot_prev
})

output$curr_plot_name <- renderText({
    curr_plot <- input$plot_next - input$plot_prev

    if(curr_plot > -1)
        return(colnames(data())[curr_plot + 2])
    else
        return(colnames(data())[2])
})

output$kinetic_fit_plot <- renderPlot({
    curr_plot  <- input$plot_next - input$plot_prev + 2
    if (curr_plot > 1)
        per_well_lightcycler(data(), curr_plot, fit_method())
    else
        per_well_lightcycler(data(), 2, fit_method())
})


observe({
    if (input$fluo_load_next == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'eff_cqs_tabpanel')
})


observe({
    if (input$eff_cqs_tabpanel_prev == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'fluo_load_tabpanel')
})


## design controller starts here


output$design_panel_browsing_buttons <- renderUI({
     
    list(
        actionButton("design_ii_tabpanel_prev", label = "Previous step",
                     additionalClass = "btn-primary"),
        actionButton("design_ii_tabpanel_next", label = "Next step",
                     additionalClass = "btn-primary")
        )    
})


observe({
    if (is.null(input$design_ii_tabpanel_next))
        return(NULL)
    if (input$design_ii_tabpanel_next == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'check_tabpanel')
})

observe({
    if (is.null(input$design_ii_tabpanel_prev))
        return(NULL)
    if (input$design_ii_tabpanel_prev == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'eff_cqs_tabpanel')
})

merge_cqs_and_user_design <- reactive({

    dat <- read.table(file.path('data', 'vermeulen_second_input.dat'), header = TRUE)
    return(dat)
      
})

output$test_dt <- renderDataTable({

    return(merge_cqs_and_user_design())
}, options = list(iDisplayLength = 5))

validate_user_design <- reactive({
    # hardcoded gene and samples genecounts, as in the fake Vermeulen's dataset
    dat <- merge_cqs_and_user_design()
    return(list(cc = 1, g = 1, r = 6, s = 1, dat = dat))
})


## ## differential expression controller start


calculate_relative_expression <- reactive({
    progress <- shiny::Progress$new()
    on.exit(progress$close())
    progress$set(message = "Processing relative expression...", value = 0)
    
    dd <- merge_cqs_and_user_design()

    lde <- relative_quantification_parsed(dd = dd,
                                          refmean = input$ref_mean,
                                          method = METHOD,
                                          type_eff = input$type_eff)
    # precomputed datasets start
    ## save(lde, file = paste0('/tmp/', 'example_lde', '_', input$ref_mean, '_',
    ##               input$type_eff, '_', input$kinetic_method, '.RData'))
    # precomputed datasets end

    return(lde)
})



output$differential_expression_plot <- renderPlot({
    load(file.path('data', sprintf('example_lde_%s_%s_%s.RData',
                                   input$ref_mean, input$type_eff, input$kinetic_method)))
                   
    plot_relative_quantification(lde[['parsed']])

    ## ## @todo remove this
    ## pdf('/tmp/differential.pdf')
    ## plot_relative_quantification(lde[['parsed']])
    ## dev.off()
})


output$differential_expression_dt <- renderDataTable({
       
    load(file.path('data', sprintf('example_lde_%s_%s_%s.RData',
                                   input$ref_mean, input$type_eff, input$kinetic_method)))
    ## moving variable names, rownames, to the first column
    cnames <- colnames(lde[['parsed']])
    rnames <- rownames(lde[['parsed']])
    
    rounded <- apply(lde[['parsed']], 2, function(x) round(x,2))

    rounded <- rbind(rounded, rep(NA, ncol(rounded)))
    rownames(rounded)[nrow(rounded)]<- 'status'
    
    for (i in 1:ncol(rounded)) {
        if ((as.numeric(rounded["perm < init", i])) < 0.05)
            rounded['status', i] <- "down"
        else if ((as.numeric(rounded["perm > init", i])) < 0.05)
            rounded['status', i] <- "up"
        else
            rounded['status', i] <- "ns"
    }
    
    out <- data.frame(cbind(contrast = cnames, t(rounded)))
    out <- out[,-9]

    colnames(out) <- c('contrast', 'Mean', 'Std.dev', 'Median', 'MAD', 'Conf.lower',
                       'Conf.upper', 'perm > init', 'perm < init', 'status')
    return(out)

})



output$stability_m <- renderPlot({
    ## foo <- calculate_stability()
    ## save(foo, file = '/tmp/example_stability.RData')
    example_stab <-  load(file.path('data', 'example_stability.RData'))
  
    stabilityM(hk = calculate_stability()$hk, ddrq = calculate_stability()$ddrq)
})


output$stability_v <- renderPlot({
  ## if ((is.null(user_design()) | is.null(cq_eff()) |
  ##       is.null(input$kinetic_method) | is.null(input$ref_mean) | is.null(input$type_eff))
  ##       &  is.null(user_design_use_case_cq_eff())) 
  ##       return(NULL)
    
    stabilityV(hk = calculate_stability()$hk)
})


output$stability_summary_text <- renderPrint({
    if ((is.null(user_design()) | is.null(cq_eff()) |
         is.null(input$kinetic_method) | is.null(input$ref_mean) | is.null(input$type_eff))
        &  is.null(user_design_use_case_cq_eff())) 
        
        return(NULL)
        
    stability_summary(hk = calculate_stability()$hk)
})


output$stability_m_pdf <-  downloadHandler(
    filename <- function() {
      sprintf('m_value_%s.pdf', get_timestamp())
    },
    content <- function(file) { 
      pdf(file, width = 10,          
          useDingbats = FALSE, bg = 'transparent')

      stabilityM(hk = calculate_stability()$hk, ddrq = calculate_stability()$ddrq)
      
      dev.off()
    },
    contentType = 'application/pdf')

## download buttons start

output$stability_m_png <-  downloadHandler(
    filename <- function() {
      sprintf('m_value_%s.png', get_timestamp())
    },
    content <- function(file) { 
      png(file)

      stabilityM(hk = calculate_stability()$hk, ddrq = calculate_stability()$ddrq)
      
      dev.off()
    },
    contentType = 'image/png')



output$stability_v_pdf <-  downloadHandler(
    filename <- function() {
      sprintf('m_value_%s.pdf', get_timestamp())
    },
    content <- function(file) { 
      pdf(file, width = 10,          
          useDingbats = FALSE, bg = 'transparent')

      stabilityV(hk = calculate_stability()$hk)
      
      dev.off()
    },
    contentType = 'application/pdf')


output$stability_v_png <-  downloadHandler(
    filename <- function() {
      sprintf('m_value_%s.png', get_timestamp())
    },
    content <- function(file) { 
      png(file)

      stabilityV(hk = calculate_stability()$hk)
      
      dev.off()
    },
    contentType = 'image/png')


output$de_csv <- downloadHandler(
    filename = function() {
        sprintf('differential_expression_%s.csv', get_timestamp())
    },
    content = function(file) {
        write.csv(x = data.frame(calculate_relative_expression()[['parsed']]), file = file, quote = FALSE)
    })



refresh_stability_input <- reactive({
    
       dat <- merge_cqs_and_user_design()

        stab_input <- stability_data_column_classes(
            data.frame(well = dat$name,
                   cq = dat$cq,
                   efficiency = dat$efficiency,
                   included = dat$included,
                   group = dat$group,
                   primers = dat$primers,
                   treatment = dat$treatment,
                   row.names = NULL))
        r <- unique(stab_input$primers[grep("^[r]", stab_input$group)])    
        rl <- length(r)

        ## validate(       
        ##     need(rl >= 3,
        ##          sprintf("Please introduce three or more reference primers to assess reference stability,
        ##               now you only entered %s as references",
        ##                  paste(unique(stab_input$primers[stab_input$primers %in% r]), collapse = ','))))
        return(stab_input)
        
})

output$de_stability_assessment <- renderUI({
    stab_input <- refresh_stability_input()
    r <- unique(refresh_stability_input()$primers[grep("^[r]", refresh_stability_input()$group)])    
    rl <- length(r)
    
    tagList(
        ## h3('Stability assessment'),
        fluidRow(
            column(width = 6,
                   h4('M values'),
                   plotOutput('stability_m'),
                   downloadButton('stability_m_png', label = 'Download as PNG'),
                   downloadButton('stability_m_pdf', label = 'Download as PDF')),
            column(width = 6,
                   h4('PV values'),
                   plotOutput('stability_v'),
                   downloadButton('stability_v_png', label = 'Download as PNG'),
                   downloadButton('stability_v_pdf', label = 'Download as PDF'))),    
        ## verbatimTextOutput("stability_summary_text"),
        uiOutput('stability_summary_html')) 
})


output$stability_summary_html <- renderUI({

    out <- stability_summary_list(hk = calculate_stability()$hk)
    ranking <- data.frame(rank = names(out$ranking), target = as.character(out$ranking)) 

    genenames <- as.vector(calculate_stability()$hk$ranking)
    plotlab <- vector()
    for(i in 1:(length(genenames)-1)){
        lab1 <- paste(genenames, collapse=", ")
        ## plotlab[i] <- paste(as.character(length(genenames)), lab1, sep=", ")
        plotlab[i] <- lab1
        genenames<-genenames[-length(genenames)]
    }

    m_values <- data.frame(number = names(out$meanM), stability = as.numeric(out$meanM), components = plotlab)

    v_values <- data.frame(step = names(out$variation), variation = as.numeric(out$variation))

    panel <- as.vector(out$panel)
    

 tagList(
     br(),
        h4('Reference stability assessment'),
        ## out$stable,
        fluidRow(
            renderDataTable(stability_self_explaining_table(hk = calculate_stability()$hk))
            )
     )
    
})


calculate_stability <- reactive({
    progress <- shiny::Progress$new()
    on.exit(progress$close())
    progress$set(message = "Processing stability...", value = 0)
    
     
    stab_input <- refresh_stability_input()

    ddrq <- stability(na.omit(stab_input))
    ## ddrq <- get_expression_quantities(na.omit(stab_input))
    hk <- select_references(ddrq)

     return(list(ddrq = ddrq, hk = hk))
     
})

## differential expression controller end


## overview tabpanel start



observe({
    if (is.null(input$check_tabpanel_next))
        return(NULL)
    if (input$check_tabpanel_next == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'de_tabpanel')
})

observe({
    if (is.null(input$check_tabpanel_prev))
        return(NULL)
    if (input$check_tabpanel_prev == 0)
        return(NULL)
    updateTabsetPanel(session, 'main', selected = 'design_tabpanel')
})




output$pce_plot <- renderPlot({
    stab_input <- refresh_stability_input()
    pce_cq_eff_plot(stab_input = na.omit(stab_input),
                    points = !input$show_pce_labels)

    ## @todo remove this
    pdf('/tmp/pce.pdf', height = 8, width = 12)
    pce_cq_eff_plot(stab_input = na.omit(stab_input),
                    points = !input$show_pce_labels)

    dev.off()

})

plot_relative_expression_quantities <- function(q) {

    dat <- list()
    dat[['nrqs']] <- t(q[['q']])
    dat[['nrqs.se']] <- t(q[['q.sd']])
    
    par(mai = par()$mai + c(0,0,0,1), lwd = 2)
    ymax <- max(dat[['nrqs']]) + max(dat[['nrqs.se']]) + 0.2
    ymin <- 0
    
    bp <- barplot(dat[['nrqs']], beside = TRUE, lwd = 2,
                  legend.text = TRUE,
                  args.legend = list( 
                      x = "topright",
                      ## inset = -0.2,
                      cex = .7),
                  ylab = 'scaled relative quantities (a.u.)',
                  ylim = c(ymin, ymax))    

    segments(bp, dat[['nrqs']] - dat[['nrqs.se']]/2 , bp,
             dat[['nrqs']] +  dat[['nrqs.se']]/2, lwd = 2, ylim = c(ymin, ymax))

    arrows(bp, dat[['nrqs']] - dat[['nrqs.se']]/2 , bp,
           dat[['nrqs']] +  dat[['nrqs.se']]/2, lwd = 2,
           angle = 90,
           code = 3, length = 0.05,
           ylim = c(ymin, ymax))
    
}

output$scaled_quantities_plot <- renderPlot({
    q <-  get_expression_quantities(refresh_stability_input())
    plot_relative_expression_quantities(q)

    ## @todo remove this
    pdf('/tmp/scaled.pdf', height = 8, width = 12)
    plot_relative_expression_quantities(q)
    dev.off()
    
})


output$scaled_quantities_dt <- renderDataTable({
    q <-  get_expression_quantities(refresh_stability_input())
    
    ## renaming colnames from, for instance, sd.act to act.stdev
    colnames(q[['q.sd']]) <- paste0(substring(colnames(q[['q.sd']]), 4, nchar(colnames(q[['q.sd']]))), '.sd')
    colnames(q[['q']]) <- paste0(colnames(q[['q']]), '.mean')
    merged <- cbind(q[['q']], q[['q.sd']])    
    merged <- round(merged[,order(names(merged))], 2)

    ## preppending the rownames
    q_names <- colnames(merged)
    merged$treatment <- row.names(merged)    
    merged <- merged[ ,c('treatment', q_names)]
    
    return(merged)    
}, options = list(pageLength = 5))


## overview tabpanel end

stability <- function(dd) {
    get_expression_quantities(dd)$q
}
