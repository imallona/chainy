#!/usr/bin/env R
##
## This file is part of chainy.
##
## chainy is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## chainy is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with chainy.  If not, see <http://www.gnu.org/licenses/>.

suppressPackageStartupMessages(library(qpcR))
library(XML)

WD <- '.'


# lightcycler parser
# extracts the fluorescence read of a given well
extract_well_lightcycler <- function(dat, i) {
    num_cycles <- dat[[i]]$points$.attrs
    name <- dat[[i]]$.attrs[1]
    fluo <- as.numeric(as.vector(na.omit(sapply(dat[[i]]$points, "[", 3))))
    to_return = list('well' = name, 'fluo' = fluo) 
     
    return(to_return) 
} 

# lightcycler parser
# parses all the 384-well plate information calling the extract_well() function
parse_input_lightcycler <- function(infile){
  tryCatch({
    doc <- xmlParse(infile)
    d = xmlRoot(doc)
    dat <- xmlToList(d)

    N_CYCLES <- as.numeric(dat[[1]]$points$.attrs[['count']])

    parsed_list <- list()
    parsed_list['Cycles'] <- list(seq(1, N_CYCLES))

    for (i in 1:length(dat)) {
        foo <- extract_well_lightcycler(dat,i)        
        parsed_list[foo$well] <- list(foo$fluo)
    }

    parsed_dat <- as.data.frame(parsed_list)

    return(parsed_dat)
    #return(list(validate = NULL, results = parsed_dat))
  ## }, error = function(x) return(stop(ed[['parse_input_lightcycler']])))
  }, error = function(x) return(NULL))
         
}


# @param i the well
# @param type  the fitting algorithm i.e. 'Cy0'
per_well_lightcycler <- function(parsed_dat, i, type) {
    foo <- try(efficiency(pcrfit(parsed_dat, 1, i), type = type), silent = T)

    ## save(foo, file = '/tmp/efficiency_by_well.RData') # remove this @todo

    if (is.list(foo))
      label <- colnames(parsed_dat)[i]
}



gapdh <- parse_input_lightcycler(file.path(WD, 'chip', 'GAPDH.xml'))

au104 <- parse_input_lightcycler(file.path(WD, 'chip', 'AU104.xml'))


## selected <- c('ACK9', 'IGG', 'H3', '1.')
## hardcode well names

wells <- c('E2..A.T.ACK9', 'E3..A.T.ACK9', 'E2..A.T.ACK9', 'E3..A.T.ACK9',
           'E8..A.T.H3', 'E9..A.T.H3', 'E8..A.T.H3', 'E9..A.T.H3',
           'E10..A.T.IGG', 'E11..A.T.IGG', 'E10..A.T.IGG', 'E11..A.T.IGG',
           'E14..A.T.1.5', 'E15..A.T.1.5', 'E14..A.T.1.5', 'E15..A.T.1.5',
           'B2..C.ACK9', 'B3..C.ACK9', 'B2..C.ACK9', 'B3..C.ACK9', 'B8..C.H3',
           'B9..C.H3', 'B8..C.H3', 'B9..C.H3', 'B10..C.IGG', 'B11..C.IGG',
           'B10..C.IGG', 'B11..C.IGG', 'B14..C.1.5', 'B15..C.1.5', 'B14..C.1.5',
           'B15..C.1.5')

gapdh_selected <- intersect(wells, colnames(gapdh))

au104_selected <- intersect(wells, colnames(au104))

foo <- gapdh[,gapdh_selected]
bar <- au104[,au104_selected]

ncycles <- 1:40
merged <- data.frame(Cycles = gapdh$Cycles)
merged <- cbind(merged, foo[ncycles,], bar[ncycles,])

write.table(merged, file = 'aza_tsa_au104_no_16cen_fluo_delim.tsv',
            quote = FALSE, sep ="\t", row.names = FALSE, col.names = TRUE)


## expression


wells2 <- c(
    'A1..96H.C', 'A2..96H.C', 'A3..96H.C', 'A16..120H.C', 'A17..120H.C',
    'A18..120H.C', 'B7..144H.C', 'B8..144H.C', 'B9..144H.C', 'E1..96H.C',
    'E2..96H.C', 'E3..96H.C', 'E16..120H.C', 'E17..120H.C', 'E18..120H.C',
    'F7..144H.C', 'F8..144H.C', 'F9..144H.C', 'I1..96H.C', 'I2..96H.C',
    'I3..96H.C', 'I16..120H.C', 'I17..120H.C', 'I18..120H.C', 'J7..144H.C',
    'J8..144H.C', 'J9..144H.C', 'M1..96H.C', 'M2..96H.C', 'M3..96H.C',
    'M16..120H.C', 'M17..120H.C', 'M18..120H.C', 'N7..144H.C', 'N8..144H.C',
    'N9..144H.C', 'B1..96H.C', 'B2..96H.C', 'B3..96H.C', 'B16..120H.C',
    'B17..120H.C', 'B18..120H.C', 'C7..144H.C', 'C8..144H.C', 'C9..144H.C',
    'B1..96H.C', 'B2..96H.C', 'B3..96H.C', 'B16..120H.C', 'B17..120H.C',
    'B18..120H.C', 'C7..144H.C', 'C8..144H.C', 'C9..144H.C', 'A10..96H.AZA.5',
    'A11..96H.AZA.5', 'A12..96H.AZA.5', 'A13..96H.AZA.10', 'A14..96H.AZA.10',
    'A15..96H.AZA.10', 'B1..120H.AZA.5', 'B2..120H.AZA.5', 'B3..120H.AZA.5',
    'B4..120H.AZA.10', 'B5..120H.AZA.10', 'B6..120H.AZA.10', 'B16..144H.AZA.5',
    'B17..144H.AZA.5', 'B18..144H.AZA.5', 'B19..144H.AZA.10', 'B20..144H.AZA.10',
    'B21..144H.AZA.10', 'E10..96H.AZA.5', 'E11..96H.AZA.5', 'E12..96H.AZA.5',
    'E13..96H.AZA.10', 'E14..96H.AZA.10', 'E15..96H.AZA.10', 'F1..120H.AZA.5',
    'F2..120H.AZA.5', 'F3..120H.AZA.5', 'F4..120H.AZA.10', 'F5..120H.AZA.10',
    'F6..120H.AZA.10', 'F16..144H.AZA.5', 'F17..144H.AZA.5', 'F18..144H.AZA.5',
    'F19..144H.AZA.10',
    'F20..144H.AZA.10', 'F21..144H.AZA.10', 'I10..96H.AZA.5', 'I11..96H.AZA.5',
    'I12..96H.AZA.5', 'I13..96H.AZA.10', 'I14..96H.AZA.10', 'I15..96H.AZA.10',
    'J1..120H.AZA.5', 'J2..120H.AZA.5', 'J3..120H.AZA.5', 'J4..120H.AZA.10',
    'J5..120H.AZA.10', 'J6..120H.AZA.10', 'J16..144H.AZA.5', 'J17..144H.AZA.5',
    'J18..144H.AZA.5', 'J19..144H.AZA.10', 'J20..144H.AZA.10', 'M10..96H.AZA.5',
    'M11..96H.AZA.5', 'M12..96H.AZA.5', 'M13..96H.AZA.10', 'M14..96H.AZA.10',
    'M15..96H.AZA.10', 'N1..120H.AZA.5', 'N2..120H.AZA.5', 'N3..120H.AZA.5',
    'N4..120H.AZA.10', 'N5..120H.AZA.10', 'N6..120H.AZA.10', 'N16..144H.AZA.5',
    'N17..144H.AZA.5', 'N18..144H.AZA.5', 'N19..144H.AZA.10', 'N20..144H.AZA.10',
    'B10..96H.AZA.5', 'B11..96H.AZA.5', 'B12..96H.AZA.5', 'B13..96H.AZA.10',
    'B14..96H.AZA.10', 'B15..96H.AZA.10', 'C1..120H.AZA.5', 'C2..120H.AZA.5',
    'C3..120H.AZA.5', 'C4..120H.AZA.10', 'C5..120H.AZA.10', 'C6..120H.AZA.10',
    'C16..144H.AZA.5', 'C17..144H.AZA.5', 'C18..144H.AZA.5', 'C19..144H.AZA.10',
    'C20..144H.AZA.10', 'C21..144H.AZA.10', 'B10..96H.AZA.5', 'B11..96H.AZA.5',
    'B12..96H.AZA.5', 'B13..96H.AZA.10', 'B14..96H.AZA.10', 'B15..96H.AZA.10',
    'C1..120H.AZA.5', 'C2..120H.AZA.5', 'C3..120H.AZA.5', 'C4..120H.AZA.10',
    'C5..120H.AZA.10', 'C6..120H.AZA.10', 'C16..144H.AZA.5', 'C17..144H.AZA.5',
    'C18..144H.AZA.5', 'C19..144H.AZA.10', 'C20..144H.AZA.10', 'C21..144H.AZA.10')



ciclof <- parse_input_lightcycler(file.path(WD, 'expression', 'CICLOF.xml'))
gldc <- parse_input_lightcycler(file.path(WD, 'expression', 'GLDC.xml'))
lncr <- parse_input_lightcycler(file.path(WD, 'expression', 'LNCR.xml'))
mrpl19 <- parse_input_lightcycler(file.path(WD, 'expression', 'MRPL19.xml'))
pmsc4 <- parse_input_lightcycler(file.path(WD, 'expression', 'PMSC4.xml'))
pum1 <- parse_input_lightcycler(file.path(WD, 'expression', 'PUM1.xml'))

merged <- NULL

merged <- data.frame(Cycles = gapdh$Cycles)

for (item in c('ciclof', 'gldc', 'lncr', 'mrpl19', 'pmsc4', 'pum1')) {
    curr <- get(item)
    selected <- intersect(wells2, colnames(curr))
    reduced <- curr[, selected]
    ## appending the gene name
    colnames(reduced) <- paste(colnames(reduced), item, sep = "_")
    assign(item, reduced[ncycles,])
    merged <- cbind(merged, get(item))
}

## intersecting

## gapdh_selected <- intersect(wells, colnames(gapdh))

## au104_selected <- intersect(wells, colnames(au104))

## foo <- gapdh[,gapdh_selected]
## bar <- au104[,au104_selected]

## ncycles <- 1:40
## merged <- data.frame(Cycles = gapdh$Cycles)
## merged <- cbind(merged, foo[ncycles,], bar[ncycles,])

write.table(merged, file = 'aza_gldc_fluo_delim.tsv',
            quote = FALSE, sep ="\t", row.names = FALSE, col.names = TRUE)



## all the chip data

gapdh <- parse_input_lightcycler(file.path(WD, 'chip', 'GAPDH.xml'))
au104 <- parse_input_lightcycler(file.path(WD, 'chip', 'AU104.xml'))
au60 <- parse_input_lightcycler(file.path(WD, 'chip', 'AU60.xml'))
cen16 <- parse_input_lightcycler(file.path(WD, 'chip', '16CEN.xml'))

merged <- NULL

merged <- data.frame(Cycles = gapdh$Cycles)
ncycles <- 1:40

for (item in c('gapdh', 'au104', 'au60', 'cen16')) {
    curr <- get(item)
    ## selected <- intersect(wells2, colnames(curr))
    selected <- grep('Cycles', colnames(curr), invert = TRUE, value = TRUE)
    ## selected <- intersect(wells, colnames(curr))    
    reduced <- curr[, selected]
    ## ## appending the XML basename
    colnames(reduced) <- paste(colnames(reduced), item, sep = "_")
    assign(item, reduced[ncycles,])
    merged <- cbind(merged, get(item))
}

write.table(merged, file = 'all_chip_data_delim.tsv',
            quote = FALSE, sep ="\t", row.names = FALSE, col.names = TRUE)
