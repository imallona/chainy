#!/usr/bin/env R

## the structured data file (provided as an attachment) full path

library(qpcR)

DAT <- 'dd.tsv'

dd <- read.table(DAT, header = TRUE, stringsAsFactors = FALSE)

str(dd)


#############################################################################
## create n-sequence (equidistant) with selected mean and s.d.  => refmean ##
makeStat <- function(n, MEAN, SD) 
{
  X <- 1:n
  Z <- (((X - mean(X, na.rm = TRUE))/sd(X, na.rm = TRUE))) * SD
  MEAN + Z  
}


refmean2 <- function(
    data, 
    group = NULL, 
    which.eff = c("sig", "sli", "exp", "mak", "ext"),
    type.eff = c("individual", "mean.single"), 
    which.cp = c("cpD2", "cpD1", "cpE", "cpR", "cpT", "Cy0", "ext"),
    verbose = TRUE,
    ...)
{
    if (class(data)[2] != "pcrbatch") stop("data must be of class 'pcrbatch'!")
    if (is.null(group)) stop("Please define 'group'!")
    if (length(group) != ncol(data) - 1) stop("Length of 'group' and 'data' do not match!")
    if (!is.numeric(which.eff)) which.eff <- match.arg(which.eff)
    type.eff <- match.arg(type.eff)
    
    ## from 1.3-7: exchange numeric efficiencies/threshold cycles
    if (is.numeric(which.eff)) {
        effDAT <- matrix(c("ext.eff", which.eff), nrow = 1) 
        colnames(effDAT) <- colnames(data)
        data <- rbind(data, effDAT)
        which.eff <- "ext" 
    } 
    if (is.numeric(which.cp)) {
        cpDAT <- matrix(c("sig.ext", which.cp), nrow = 1) 
        colnames(cpDAT) <- colnames(data)
        data <- rbind(data, cpDAT)
        which.cp <- "ext" 
    }
    
    ## get 'group' name
    GROUPNAME <- deparse(substitute(group))  
    
    ## split labels and data
    ANNO <- data[, 1, drop = FALSE]
    DATA <- data[, -1, drop = FALSE]

    head(ANNO)
    head(DATA)
    
    ## check for equal length of data and 'group' vector
    if (length(group) != ncol(DATA)) stop("Length of 'group' and 'data' do not match!")
    
    ## check for number of reference genes, control samples and treatment samples
    numCon <- unique(na.omit(as.numeric(sub("g\\d*c(\\d*)", "\\1", group, perl = TRUE))))
    numSamp <- unique(na.omit(as.numeric(sub("g\\d*s(\\d*)", "\\1", group, perl = TRUE))))  
    RefInCon <- unique(na.omit(as.numeric(sub("r(\\d*)c\\d*", "\\1", group, perl = TRUE))))
    RefInSamp <- unique(na.omit(as.numeric(sub("r(\\d*)s\\d*", "\\1", group, perl = TRUE))))
    
    ## initialize variables  
    matMEAN <- NULL
    matSD <- NULL
    allSEL <- NULL 
    
    ## averaging of reference genes in control and treatment samples 
    for (k in c("c", "s")) {
        for (i in switch(k, "c" = numCon, "s" = numSamp)) {
            if (verbose) cat(" Calculating statistics (mean & sd) for: ")
            for (j in switch(k, "c" = RefInCon, "s" = RefInSamp)) {

                
                ## STR <- paste("r", j, k, i, sep = "")
                ## to avoid substring matching
                STR <- paste("r", j, k, i,  sep = "")   

                ## @todo remove this
                ## print(STR)

                if (verbose) cat(STR, "")
                SEL <- grep(paste0(STR, "$"), group)       
                allSEL <- c(allSEL, SEL)
                tempDAT <- DATA[, SEL, drop = FALSE]
                GROUP <- as.factor(rep(1, ncol(tempDAT)))  
                MEAN <- apply(tempDAT, 1, function(x) tapply(as.numeric(x), GROUP, function(y) mean(y, na.rm = TRUE)))
                MEAN <- matrix(MEAN)      
                colnames(MEAN) <- STR      
                SD <- apply(tempDAT, 1, function(x) tapply(as.numeric(x), GROUP, function(y) sd(y, na.rm = TRUE)))
                SD <- matrix(SD)    
                
                ## replace NA values in SD that occur when sd is done on single runs
                if (all(is.na(SD))) SD <- matrix(0, nrow = nrow(SD), ncol = 1)
                
                ## if reference values are taken as all with the same average or is numeric, set s.d. = 0 
                if (type.eff == "mean.single" || is.numeric(which.eff)) SD <- matrix(rep(0, nrow(SD)))
                colnames(SD) <- STR
                matMEAN <- cbind(matMEAN, MEAN)
                matSD <- cbind(matSD, SD)
                ## print(head(matSD))
            }
            
            if (verbose) cat("\n")
            
            TEXT1 <- paste("a", 1:ncol(matMEAN), sep = "", collapse = " + ")
            TEXT2 <- paste("(", TEXT1, ")", "/", ncol(matMEAN), sep = "")
            EXPR <- parse(text = TEXT2)  
            
            ## error propagation of averaged efficiencies
            selEFF <- which(ANNO == paste(which.eff, "eff", sep = "."))      
            meanEFF <- matMEAN[selEFF, , drop = FALSE]      
            sdEFF <- matSD[selEFF, ]     
            statEFF <- rbind(meanEFF, sdEFF) 
            if (verbose) cat("  => error propagation for", colnames(statEFF), "(efficiencies)...\n")
            colnames(statEFF) <- paste("a", 1:ncol(statEFF), sep = "")    
            propEFF <- propagate(EXPR, statEFF, type = "stat", plot = FALSE, ...)
            ## print(propEFF)
            ## save.image('/tmp/foo.RData')
            
            newEFF <- makeStat(length(allSEL), propEFF$summary[1, "Prop"], propEFF$summary[2, "Prop"])
            if (verbose) cat("  => replacing with new values...\n")

            ## print(selEFF)
            ## print(allSEL)

            DATA[selEFF, allSEL] <- round(newEFF, 6)
            
            ## error propagation of averaged threshold cycles
            selCP <- which(ANNO == paste("sig", which.cp, sep = "."))
            meanCP <- matMEAN[selCP, ]
            sdCP <- matSD[selCP, ]
            statCP <- rbind(meanCP, sdCP)   
            if (verbose) cat("  => error propagation for", colnames(statCP), "(threshold cycles)...\n")
            colnames(statCP) <- paste("a", 1:ncol(statCP), sep = "")    
            propCP <- propagate(EXPR, statCP, type = "stat", plot = FALSE, ...)
            newCP <- makeStat(length(allSEL), propCP$summary[1, "Prop"], propCP$summary[2, "Prop"])
            if (verbose) cat("  => replacing with new values...\n")
            DATA[selCP, allSEL] <- round(newCP, 2)
            
            ## exchange labels in 'group' vector    
            ## POS <- grep(paste("r.", k, i, sep = ""), group)
            POS <- grep(paste("r.", k, i, "$", sep = ""), group)
            group[POS]  <- paste("r1", k, i, sep = "")   
            
            ## reset variables
            matMEAN <- NULL
            matSD <- NULL
            allSEL <- NULL
        }
    }
    
    DATA <- cbind(ANNO, DATA)
    class(DATA) <- c("data.frame", "pcrbatch")
    
    ## attaching 'group' attribute for 'ratiobatch'
    attr(DATA, "group") <- group
    return(DATA)    
}





ratiobatch2 <- function(
data, 
group = NULL, 
plot = TRUE, 
combs = c("same", "across", "all"),
type.eff = "mean.single",
which.cp = "cpD2",
which.eff = "sli",
refmean2 = FALSE,
dataout = NULL, 
verbose = TRUE,
...)
{
  if (class(data)[2] != "pcrbatch") stop("data is not of class 'pcrbatch'!")
  options(warn = -1)

  combs <- match.arg(combs)
  
  ## substitute .1 etc from import 
  group <- sub("\\.\\d*", "", group, perl = TRUE)
  
  NCOL <- ncol(data) - 1
  
  ## from 1.3-7: added option of external efficiencies or threshold cycles,
  ## either single value (recycled) or a vector of values.  
  if (is.numeric(which.eff)) {    
    if (length(which.eff) == 1) which.eff <- rep(which.eff, NCOL)
    else {
      if (length(which.eff) != NCOL) stop("Length of input efficiencies does not match number of runs!")
    }    
    effDAT <- matrix(c("extEFF", which.eff), nrow = 1)
    colnames(effDAT) <- colnames(data)
    data <- rbind(data, effDAT) 
    extEFF <- TRUE
  } else extEFF <- FALSE
  
  if (is.numeric(which.cp)) {
    if (length(which.cp) != NCOL) stop("Length of input threshold cycles does not match number of runs!")
    cpDAT <- matrix(c("extCP", which.cp), nrow = 1)
    colnames(cpDAT) <- colnames(data)
    data <- rbind(data, cpDAT)     
    extCP <- TRUE
  } else extCP <- FALSE
  
  ANNO <- data[, 1, drop = FALSE]  
  DATA <- data[, -1, drop = FALSE] 
    
  if (verbose) cat("\nChecking if sample number and 'group' length are equal...")
  if (length(group) != NCOL) stop("Length of 'group' and 'data' do not match!")
  
  ## take column names if 'group' is empty
  if (is.null(group)) {
    group <- colnames(DATA)
    if (verbose) cat("'group' is NULL: using column names...")
  }
  
  ## test for character of 'group' definition
  if (verbose) cat("\nChecking that 'group' is of class <character>...")
  CLASS <- sapply(group, function(x) class(x))
  if (!all(CLASS == "character")) stop("'group' definition must be of class <character> (i.e. r1g1)")
  
  ## check for number of control samples, treatment samples, genes-of-interest and reference genes, 
  if (verbose) cat("\nChecking for number of control samples, treatment samples, \ngenes-of-interest and reference genes:\n")
  
  numCon <- unique(na.omit(as.numeric(sub("g\\d*c(\\d*)", "\\1", group, perl = TRUE))))
  if (verbose) cat(" Found", length(numCon), "control sample(s)...\n")
  numSamp <- unique(na.omit(as.numeric(sub("g\\d*s(\\d*)", "\\1", group, perl = TRUE))))  
  if (verbose) cat(" Found", length(numSamp), "treatment sample(s)...\n")
  GoiInCon <- unique(na.omit(as.numeric(sub("g(\\d*)c\\d*", "\\1", group, perl = TRUE))))
  if (verbose) cat(" Found", length(GoiInCon), "genes-of-interest in control sample(s)...\n")
  GoiInSamp <- unique(na.omit(as.numeric(sub("g(\\d*)s\\d*", "\\1", group, perl = TRUE))))
  if (verbose) cat(" Found", length(GoiInSamp), "genes-of-interest in treatment sample(s)...\n")
  RefInCon <- unique(na.omit(as.numeric(sub("r(\\d*)c\\d*", "\\1", group, perl = TRUE))))
  if (verbose) cat(" Found", length(RefInCon), "reference genes in control sample(s)...\n")
  RefInSamp <- unique(na.omit(as.numeric(sub("r(\\d*)s\\d*", "\\1", group, perl = TRUE)))) 
  if (verbose) cat(" Found", length(RefInSamp), "reference genes in treatment sample(s)...\n")
  
  ## check equal use of genes-of-interest/reference genes in treatments/controls
  if (!all(RefInCon == RefInSamp)) stop("Unequal number of reference genes in treatment and control samples!")
  if (!all(GoiInCon == GoiInSamp)) stop("Unequal number of genes-of-interest in treatment and control samples!") 
  
  ## from 1.3-7: added removal of failed runs (either failed fits
  ## or SOD outlier) from DATA and 'group' by identification
  ## of *...* or **...** in sample name
  sampNAMES <- names(DATA)
  hasTag <- grep("\\*[[:print:]]*\\*", sampNAMES)
  if (length(hasTag) > 0) {
    DATA <- DATA[, -hasTag]
    group <- group[-hasTag]
    data <- cbind(ANNO, DATA)
  }
  
  ## from 1.3-6: pass data to 'refmean2' for averaging of 
  ## multiple reference genes, and then use modified 'group' label
  ## in attributes
  if (refmean2) {
    if (all(RefInCon <= 1) && all(RefInSamp <= 1)) {
      cat(" Less than two reference genes found. Skipping 'refmean2'...\n")     
    } else {
      cat("Averaging reference genes:\n")     
      data <- refmean2(data = data, group = group, which.eff = which.eff,  
                      which.cp = which.cp, verbose = verbose)
      group <- attr(data, "group")
    }
  }
    
  ## detect r*c*, g*c*, r*s* and g*s* in 'group'
  RCs <- sort(unique(grep("r\\d*c\\d*", group, perl = TRUE, value = TRUE)))
  GCs <- sort(unique(grep("g\\d*c\\d*", group, perl = TRUE, value = TRUE)))
  RSs <- sort(unique(grep("r\\d*s\\d*", group, perl = TRUE, value = TRUE)))
  GSs <- sort(unique(grep("g\\d*s\\d*", group, perl = TRUE, value = TRUE)))
  
  ## detect absence of reference runs
  if (length(RCs) > 0 && length(RSs) > 0) hasRef <- TRUE else hasRef <- FALSE  
      
  ## do combinations in presence/absence of reference genes
  if (hasRef) COMBS <- expand.grid(GCs, GSs, RCs, RSs, stringsAsFactors = FALSE)   
  else COMBS <- expand.grid(GCs, GSs, stringsAsFactors = FALSE) 
  
  ## remove 'nonpair' combinations, i.e. r1s2:r1s1    
  Cnum <- t(apply(COMBS, 1, function(x) gsub("[rgs]\\d*", "", x, perl = TRUE)))      
  Snum <- t(apply(COMBS, 1, function(x) gsub("[rgc]\\d*", "", x, perl = TRUE)))
  Gnum <- t(apply(COMBS, 1, function(x) gsub("[rsc]\\d*", "", x, perl = TRUE)))
  Rnum <- t(apply(COMBS, 1, function(x) gsub("[gsc]\\d*", "", x, perl = TRUE)))              
  
  Cnum <- t(apply(Cnum, 1, function(x) x[x != ""]))
  Snum <- t(apply(Snum, 1, function(x) x[x != ""]))
  Gnum <- t(apply(Gnum, 1, function(x) x[x != ""]))
  Rnum <- t(apply(Rnum, 1, function(x) x[x != ""]))   
  
  ## select combinations as set under 'combs'
  if (hasRef) {     
    if (combs == "across") {
      SELECT <- which(Cnum[, 1] == Cnum[, 2] & Snum[, 1] == Snum[, 2])
      COMBS <- COMBS[SELECT, ]
    } else 
    if (combs == "same") {
      SELECT <- which(Cnum[, 1] == Cnum[, 2] & Snum[, 1] == Snum[, 2] & Gnum[, 1] == Gnum[, 2] & Rnum[, 1] == Rnum[, 2])  
      COMBS <- COMBS[SELECT, ]
    } 
  } else {
    if (combs == "same") {       
      SELECT <- which(Gnum[, 1] == Gnum[, 2])       
      COMBS <- COMBS[SELECT, ]
    }        
  }         
  
  ncomb <- nrow(COMBS)
  outLIST <- list()
  outDATA <- list()
  nameLIST <- list()             
  counter <- 1       
  
  ## take combinations into dataframe  
  for (i in 1:nrow(COMBS)) {    
    if (hasRef) {
      GCmatch <- grep(COMBS[i, 1], group, perl = TRUE)     
      GCdat <- as.data.frame(DATA[, GCmatch])
      GSmatch <- grep(COMBS[i, 2], group, perl = TRUE)     
      GSdat <- as.data.frame(DATA[, GSmatch])
      RCmatch <- grep(COMBS[i, 3], group, perl = TRUE)         
      RCdat <- as.data.frame(DATA[, RCmatch]) 
      RSmatch <- grep(COMBS[i, 4], group, perl = TRUE)     
      RSdat <- as.data.frame(DATA[, RSmatch])
      finalDATA <- cbind(ANNO, GCdat, GSdat, RCdat, RSdat)         
    } else {          
      GCmatch <- grep(COMBS[i, 1], group, perl = TRUE)
      GCdat <- as.data.frame(DATA[, GCmatch])
      GSmatch <- grep(COMBS[i, 2], group, perl = TRUE)     
      GSdat <- as.data.frame(DATA[, GSmatch])          
      finalDATA <- cbind(ANNO, GCdat, GSdat) 
    }   
    
    ## from 1.3-7: subset external efficiencies/threshold cycles
    if (extEFF) {
      selEFF <- which(ANNO == "extEFF")
      which.eff <- as.numeric(finalDATA[selEFF, -1])
    }    
    if (extCP) which.cp <- as.numeric(finalDATA[nrow(finalDATA), -1])  
    
    ## Naming by 'rs' type
    finalNAME <-  rev(as.vector(unlist(COMBS[i, ])))
    finalNAME <- paste(finalNAME, collapse = ":")      
    if (verbose) cat("Calculating ", finalNAME, " (", counter, " of ", ncomb, ")...\n", sep = "")
    flush.console()
    class(finalDATA) <- c("data.frame", "pcrbatch")
    if (hasRef) finalGROUP <- c(rep("gc", ncol(GCdat)), rep("gs", ncol(GSdat)), rep("rc", ncol(RCdat)), rep("rs", ncol(RSdat)))
    else finalGROUP <- c(rep("gc", ncol(GCdat)), rep("gs", ncol(GSdat)))    
            
    ## do ratio calculation for all combinations       
    outALL <- ratiocalc(finalDATA, finalGROUP, plot = plot, type.eff = type.eff, 
                        which.cp = which.cp, which.eff = which.eff, ...)
    
    if (!is.null(nrow(outALL$data.Sim))) SIMS <- outALL$data.Sim[, "resSIM"] else SIMS <- NULL
    if (!is.null(nrow(outALL$data.Perm))) PERMS <- outALL$data.Perm[, "resPERM"] else PERMS <- NULL
    
    PROPS <- outALL$data.Prop    
    outDATA[[counter]] <- cbind.na(SIMS, PERMS, PROPS)
    outLIST[[counter]] <- outALL$summary
    nameLIST[[counter]] <- finalNAME
    counter <- counter + 1 
  }  
  
  ## create method names for values
  names(outLIST) <- nameLIST
  outFRAME <- sapply(outLIST, function(x) as.matrix(x, ncol = 1))
  rowNAMES <- c(paste(rownames(outLIST[[1]]), "Sim", sep = "."), 
                paste(rownames(outLIST[[1]]), "Perm", sep = "."), 
                paste(rownames(outLIST[[1]]), "Prop", sep = ".")) 
  rownames(outFRAME) <- rowNAMES 
  outFRAME <- outFRAME[complete.cases(outFRAME), , drop = FALSE]   
  
  ## plot results
  if (plot && length(outLIST) < 50) {
    DIM <- ceiling(sqrt(length(outLIST)))
    par(mfrow = c(DIM, DIM + 1)) 
    par(mar = c(1, 4, 2, 2))
    for (i in 1:length(outLIST)) {
      outDATA[[i]][outDATA[[i]] <= 0] <- NA
      YLIM <- c(min(outDATA[[i]], na.rm = TRUE), max(outDATA[[i]], na.rm = TRUE))
      boxplot(outDATA[[i]], col = c("darkblue", "darkred", "darkgreen"), outline = FALSE,
               main = nameLIST[[i]], cex.main = 0.8, names = FALSE, las = 2, log = "y", ylim = YLIM)
      CONF <- apply(outDATA[[i]], 2, function(x) quantile(x, c(0.025, 0.975), na.rm = TRUE))
      COLS <- 1:ncol(CONF)
      segments(COLS - 0.2, CONF[1, ], COLS + 0.2, CONF[1, ], col = c("darkblue", "darkred", "darkgreen"), lwd = 2)
      segments(COLS - 0.2, CONF[2, ], COLS + 0.2, CONF[2, ], col = c("darkblue", "darkred", "darkgreen"), lwd = 2)
    
    }
    par(mar = c(0.5, 0.5, 0.5, 0.5))
    plot(1, 1, type = "n", axes = FALSE)
    legend(x = 0.7, y = 1.4, legend = c("Monte-Carlo\nSimulation", "Permutation", "Error\nPropagation"), 
           bty = "n", cex = 1, fill  = c("darkblue", "darkred", "darkgreen"), y.intersp = 1)
  } 
  
  outFRAME2 <- cbind(VALS = rownames(outFRAME), outFRAME)
  if (!is.null(dataout)) write.table(outFRAME2, dataout, sep = "\t", row.names = FALSE)
  return(list(resList = outLIST, resDat = outFRAME))     
}

## utils start

######################################################################
## cbind modification without replication ############################
cbind.na <- function (..., deparse.level = 1) 
{
    na <- nargs() - (!missing(deparse.level))    
    deparse.level <- as.integer(deparse.level)
    stopifnot(0 <= deparse.level, deparse.level <= 2)
    argl <- list(...)   
    while (na > 0 && is.null(argl[[na]])) {
        argl <- argl[-na]
        na <- na - 1
    }
    if (na == 0) 
        return(NULL)
    if (na == 1) {         
        if (isS4(..1)) 
            return(cbind2(..1))
        else return(matrix(...))  ##.Internal(cbind(deparse.level, ...)))
    }    
    
    if (deparse.level) {       
        symarg <- as.list(sys.call()[-1L])[1L:na]
        Nms <- function(i) {
            if (is.null(r <- names(symarg[i])) || r == "") {
                if (is.symbol(r <- symarg[[i]]) || deparse.level == 
                  2) 
                  deparse(r)
            }
            else r
        }
    }   
    ## deactivated, otherwise no fill in with two arguments
    if (na == 0) {
        r <- argl[[2]]
        fix.na <- FALSE
    }
    else {
        nrs <- unname(lapply(argl, nrow))
        iV <- sapply(nrs, is.null)
        fix.na <- identical(nrs[(na - 1):na], list(NULL, NULL))
        ## deactivated, otherwise data will be recycled
        #if (fix.na) {
        #    nr <- max(if (all(iV)) sapply(argl, length) else unlist(nrs[!iV]))
        #    argl[[na]] <- cbind(rep(argl[[na]], length.out = nr), 
        #        deparse.level = 0)
        #}       
        if (deparse.level) {
            if (fix.na) 
                fix.na <- !is.null(Nna <- Nms(na))
            if (!is.null(nmi <- names(argl))) 
                iV <- iV & (nmi == "")
            ii <- if (fix.na) 
                2:(na - 1)
            else 2:na
            if (any(iV[ii])) {
                for (i in ii[iV[ii]]) if (!is.null(nmi <- Nms(i))) 
                  names(argl)[i] <- nmi
            }
        }
           
        ## filling with NA's to maximum occuring nrows
        nRow <- as.numeric(sapply(argl, function(x) NROW(x)))
        maxRow <- max(nRow, na.rm = TRUE)  
        argl <- lapply(argl, function(x)  if (is.null(nrow(x))) c(x, rep(NA, maxRow - length(x)))
                                          else rbind.na(x, matrix(, maxRow - nrow(x), ncol(x))))
        r <- do.call(cbind, c(argl[-1L], list(deparse.level = deparse.level)))
    }
    d2 <- dim(r)
    r <- cbind2(argl[[1]], r)
    if (deparse.level == 0) 
        return(r)
    ism1 <- !is.null(d1 <- dim(..1)) && length(d1) == 2L
    ism2 <- !is.null(d2) && length(d2) == 2L && !fix.na
    if (ism1 && ism2) 
        return(r)
    Ncol <- function(x) {
        d <- dim(x)
        if (length(d) == 2L) 
            d[2L]
        else as.integer(length(x) > 0L)
    }
    nn1 <- !is.null(N1 <- if ((l1 <- Ncol(..1)) && !ism1) Nms(1))
    nn2 <- !is.null(N2 <- if (na == 2 && Ncol(..2) && !ism2) Nms(2))
    if (nn1 || nn2 || fix.na) {
        if (is.null(colnames(r))) 
            colnames(r) <- rep.int("", ncol(r))
        setN <- function(i, nams) colnames(r)[i] <<- if (is.null(nams)) 
            ""
        else nams
        if (nn1) 
            setN(1, N1)
        if (nn2) 
            setN(1 + l1, N2)
        if (fix.na) 
            setN(ncol(r), Nna)
    }
    r
}

##############################################################
## rbind modification without replication ####################
rbind.na <- function (..., deparse.level = 1) 
{
    na <- nargs() - (!missing(deparse.level))
    deparse.level <- as.integer(deparse.level)
    stopifnot(0 <= deparse.level, deparse.level <= 2)
    argl <- list(...)
    while (na > 0 && is.null(argl[[na]])) {
        argl <- argl[-na]
        na <- na - 1
    }    
    if (na == 0) 
        return(NULL)
    if (na == 1) {
        if (isS4(..1)) 
            return(rbind2(..1))
        else return(matrix(..., nrow = 1)) ##.Internal(rbind(deparse.level, ...)))
    }
        
    if (deparse.level) {
        symarg <- as.list(sys.call()[-1L])[1L:na]
        Nms <- function(i) {
            if (is.null(r <- names(symarg[i])) || r == "") {
                if (is.symbol(r <- symarg[[i]]) || deparse.level == 
                  2) 
                  deparse(r)
            }
            else r
        }
    }
    
    ## deactivated, otherwise no fill in with two arguments
    if (na == 0) {
        r <- argl[[2]]
        fix.na <- FALSE
    }
    else {
        nrs <- unname(lapply(argl, ncol))
        iV <- sapply(nrs, is.null)
        fix.na <- identical(nrs[(na - 1):na], list(NULL, NULL))
        ## deactivated, otherwise data will be recycled
        #if (fix.na) {
        #    nr <- max(if (all(iV)) sapply(argl, length) else unlist(nrs[!iV]))
        #    argl[[na]] <- rbind(rep(argl[[na]], length.out = nr), 
        #        deparse.level = 0)
        #}
        if (deparse.level) {
            if (fix.na) 
                fix.na <- !is.null(Nna <- Nms(na))
            if (!is.null(nmi <- names(argl))) 
                iV <- iV & (nmi == "")
            ii <- if (fix.na) 
                2:(na - 1)
            else 2:na
            if (any(iV[ii])) {
                for (i in ii[iV[ii]]) if (!is.null(nmi <- Nms(i))) 
                  names(argl)[i] <- nmi
            }
        }
        
        ## filling with NA's to maximum occuring ncols
        nCol <- as.numeric(sapply(argl, function(x) if (is.null(ncol(x))) length(x)
                                                    else ncol(x)))
        maxCol <- max(nCol, na.rm = TRUE)  
        argl <- lapply(argl, function(x)  if (is.null(ncol(x))) c(x, rep(NA, maxCol - length(x)))
                                          else cbind(x, matrix(, nrow(x), maxCol - ncol(x))))  
        
        ## create a common name vector from the
        ## column names of all 'argl' items
        namesVEC <- rep(NA, maxCol)  
        for (i in 1:length(argl)) {
          CN <- colnames(argl[[i]])          
          m <- !(CN %in% namesVEC)
          namesVEC[m] <- CN[m]          
        }  
        
        ## make all column names from common 'namesVEC'
        for (j in 1:length(argl)) {    
          if (!is.null(ncol(argl[[j]]))) colnames(argl[[j]]) <- namesVEC
        }
        
        r <- do.call(rbind, c(argl[-1L], list(deparse.level = deparse.level)))        
    }
    
    d2 <- dim(r)
    
    ## make all column names from common 'namesVEC'
    colnames(r) <- colnames(argl[[1]])
    
    r <- rbind2(argl[[1]], r)
        
    if (deparse.level == 0) 
        return(r)
    ism1 <- !is.null(d1 <- dim(..1)) && length(d1) == 2L
    ism2 <- !is.null(d2) && length(d2) == 2L && !fix.na
    if (ism1 && ism2) 
        return(r)
    Nrow <- function(x) {
        d <- dim(x)
        if (length(d) == 2L) 
            d[1L]
        else as.integer(length(x) > 0L)
    }
    nn1 <- !is.null(N1 <- if ((l1 <- Nrow(..1)) && !ism1) Nms(1))
    nn2 <- !is.null(N2 <- if (na == 2 && Nrow(..2) && !ism2) Nms(2))
    if (nn1 || nn2 || fix.na) {
        if (is.null(rownames(r))) 
            rownames(r) <- rep.int("", nrow(r))
        setN <- function(i, nams) rownames(r)[i] <<- if (is.null(nams)) 
            ""
        else nams
        if (nn1) 
            setN(1, N1)
        if (nn2) 
            setN(1 + l1, N2)
        if (fix.na) 
            setN(nrow(r), Nna)
    }
    r
}

## utils end


bar <- rbind(dd$efficiency, dd$cq)
colnames(bar) <- dd$group
bar <- as.data.frame(bar)
bar <- cbind(Vars = c("sig.eff", "sig.cpD2"), bar)
class(bar)[2] <- "pcrbatch"


ratiobatch2(bar, dd$group, which.cp = "cpD2",
           which.eff = "sig", refmean = TRUE)




## data(vermeulen1)

## foo <- pcrbatch(vermeulen2, fluo=2:(length(dd$group)+1), model = l5)

## foo[8,-1] <- dd$cq
## foo[1,-1] <- dd$efficiency
## colnames(foo) <- c('Vars', dd$name)

## ratiobatch2(foo, dd$group, which.cp = "cpD2",
##            which.eff = "sig", refmean = TRUE)


## ratiobatch(foo, dd$group, which.cp = "cpD2",
##            which.eff = "sig", refmean = TRUE)
