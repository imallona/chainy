# Dockerfile for shiny
#
# R 3.6.3 shiny-server app exposed in 4040
# 
# Izaskun Mallona
# 17 Aug 2020

# docker build -t chainy .
# docker run --rm -p 4040:3838 --name chainy chainy
# docker exec -it chainy /bin/bash

FROM rocker/shiny:4.0.2

# system libraries of general use
RUN apt-get update && apt-get install -y \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    git \
    libxml2 \
    libxml2-dev \
    r-cran-xml \
    r-cran-rgl \
    libglu1-mesa-dev 
  

# install R packages required

# RUN R -e "install.packages('devtools', repos='http://cran.rstudio.com/')"

RUN R -e "install.packages('BiocManager', repos='http://cran.rstudio.com/')"

RUN R -e "BiocManager::install(c('shiny', 'shinyTable', 'markdown', 'qpcR', 'XML', 'RDML', 'NormqPCR'))"

# code

RUN git clone https://bitbucket.org/imallona/chainy

RUN mv chainy /srv/shiny-server

RUN cd /srv/shiny-server/chainy

COPY shiny-server.conf /etc/shiny-server

# # select port
 EXPOSE 3838

# # allow permission
RUN sudo chown -R shiny:shiny /srv/shiny-server
RUN sudo chown -R shiny:shiny /var/lib/shiny-server

# # run app
#  CMD ["/usr/bin/shiny-server.sh"]

